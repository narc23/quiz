<?php
ini_set('display_errors', 1);
require_once('vendor/autoload.php');

try {
    session_start();
    $controller = new Classes\Controller\QuizController();
    echo $controller->dispatch();
} catch (Exception $exception) {
    echo $exception->getMessage() . $exception->getCode();
}
