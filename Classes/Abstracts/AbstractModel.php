<?php

namespace Classes\Abstracts;

use PDO;

class AbstractModel
{

    protected $databaseConnection = null;

    /**
     * @param PDO $databaseConnection
     */
    public function __construct(PDO $databaseConnection)
    {
        $this->databaseConnection = $databaseConnection;
    }

    /**
     * @param $table
     *
     * @return array
     */
    public function findAll($table): array
    {
        $sqlPattern = 'SELECT * FROM %s';
        $sql = sprintf($sqlPattern, $table);
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->execute();
        $result = $preparedQuery->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * @param int           $id
     * @param string|string $table
     *
     * @return array
     */
    public function findById(int $id, string $table): array
    {
        $sqlPattern = 'SELECT * FROM %s WHERE id = :id';
        $sql = sprintf($sqlPattern, $table);
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->bindValue(':id', $id, PDO::PARAM_INT);
        $preparedQuery->execute();
        $result = $preparedQuery->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }
}