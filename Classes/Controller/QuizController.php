<?php

namespace Classes\Controller;

use Classes\Model\QuizModel;
use Classes\Utility\ConfigurationHandler;
use Classes\Utility\DatabaseHandler;
use Classes\View\View;

class QuizController
{

    /** @var array */
    private $config = [];

    /** @var QuizModel */
    private $model = null;

    /** @var View */
    private $view = null;

    /**
     * @throws \Exception
     *
     */
    public function __construct()
    {
        $this->model = new QuizModel(DatabaseHandler::getDatabaseConnection());
        $this->view = new View();
        $this->config = ConfigurationHandler::getConfiguration('QuizConfig');
        if (empty($_SESSION['quiz']['initialized'])) {
            $this->initialize();
        }
    }

    private function initialize()
    {
        if (empty($this->config['set_id'])) {
            throw new \Exception('No Question Set ID defined in config.yaml', 1607025719);
        }

        $randomize = false;
        if (!empty($this->config['randomize']) && $this->config['randomize'] === true) {
            $randomize = true;
        }

        $result = $this->model->getQuestionSetIdsBySetId($this->config['set_id'], $randomize);

        if (empty($result)) {
            throw new \Exception('No questions available', 1607027267);
        }
        $_SESSION['quiz']['available_questions'] = $result;
        $_SESSION['quiz']['number_of_questions'] = count($result);
        $_SESSION['quiz']['initialized'] = true;
    }

    /**
     * Main entry point
     *
     * @throws Exception
     * @return string
     */
    public function dispatch(): string
    {
        $action = !empty($_REQUEST['action']) ? $_REQUEST['action'] : 'showIntro';
        $actionName = $action . 'Action';
        if (method_exists($this, $actionName) && is_callable([$this, $actionName])) {
            return $this->$actionName();
        } else {
            throw new \BadMethodCallException('Method called ( ' . $actionName . ' ) does not exists or is not callable in Class: ' . get_called_class() . ' !',
                1495744305);
        }
    }

    /**
     * @return string
     */
    protected function showIntroAction(): string
    {
        $result = $this->model->getQuestionSet($this->config['set_id']);

        return $this->view->setTemplate('Partials/intro.html')
            ->setVariables(
                [
                    'description' => $result[0]['description'],
                    'msg' => !empty($_REQUEST['msg']) ?: null
                ]
            )
            ->render();
    }

    /**
     * @return string
     */
    protected function showQuestionAction(): string
    {
        $this->checkSessionAndRedirect();

        if ((int)$_REQUEST['q'] == $_SESSION['quiz']['number_of_questions']) {
            header('Location: /?action=showSummary');
        }
        $id = (int)$_REQUEST['q'];
        $questionId = $_SESSION['quiz']['available_questions'][$id];
        $question = $this->model->findById($questionId, 'questions');
        $answers = $this->model->getAnswersByQuestionId($questionId);
        $questionData = [
            'next_question_count' => ($id + 1),
            'current_question_count' => $id,
            'current_question_id' => $questionId
        ];

        $result = array_merge($question[0], $questionData, ['answers' => $answers]);

        return $this->view->setTemplate('Partials/question.html')
            ->setVariables($result)
            ->render();
    }

    /**
     * @return string
     */
    protected function showSummaryAction(): string
    {
        $this->checkSessionAndRedirect();

        $summary = [];
        $questions = $_SESSION['quiz']['available_questions'];
        $total = $_SESSION['quiz']['number_of_questions'];
        $participantsId = $_SESSION['quiz']['participant'];
        $score = 0;
        $i = 0;

        foreach ($questions as $questionId) {
            $summary[$i]['question'] = $this->model->findById($questionId, 'questions');
            $answers = $this->model->getAnswersByQuestionId($questionId);

            $givenAnswers = $this->model->getGivenAnswersByParticipantAndQuestionId($participantsId, $questionId);
            $givenAnswerIds = explode(',', $givenAnswers[0]['given_answer_ids']);
            $correctAnswersIds = explode(',', $givenAnswers[0]['correct_answer_ids']);

            if ($this->evaluateAnswers($correctAnswersIds, $givenAnswerIds)) {
                $score++;
                $summary[$i]['points'] = 1;
            } else {
                $summary[$i]['points'] = 0;
            }

            foreach ($answers as &$answer) {
                $answer['answer_given_by_participant'] = false;
                if (in_array($answer['id'], $givenAnswerIds)) {
                    $answer['answer_given_by_participant'] = true;
                }
            }
            $summary[$i]['answers'] = $answers;
            $i++;
        }
        $summaryScore['score'] = $score;
        $summaryScore['total'] = $total;
        $this->model->setScore($_SESSION['quiz']['participant'], $this->config['set_id'], $score);
        $_SESSION['quiz']['summary_done'] = true;

        return $this->view->setTemplate('Partials/summary.html')->setVariables([
            'summary' => $summary,
            'summary_score' => $summaryScore
        ])->render();
    }

    /**
     * @return string
     */
    protected function showHighScoreAction(): string
    {
        $result = $this->model->getAllParticipantsBySetId($this->config['set_id']);

        return $this->view->setTemplate('Partials/highscore.html')->setVariables([
            'highscore' => $result,
            'show_summary_link' => !empty($_SESSION['quiz']['summary_done'])
        ])->render();
    }

    /**
     * Kills session and redirects to intro
     */
    protected function startNewQuizAction()
    {
        session_destroy();
        header('Location: /?action=showIntro');
    }

    /**
     * Saves participant and redirects either to first question or intro on failure
     */
    protected function saveParticipantAction()
    {
        if (empty($this->model->getParticipant($_POST['name'], $this->config['set_id']))) {
            $_SESSION['quiz']['participant'] = $this->model->setParticipant($_POST['name'], $this->config['set_id']);
            header('Location: /?action=showQuestion&q=0');
        } else {
            header('Location: /?action=showIntro&msg=participant');
        }

    }

    /**
     * Saves answers and redirects either to next question or shows
     * failure page if question was already answered
     */
    protected function saveAnswersAction()
    {
        $participant = $_SESSION['quiz']['participant'];
        $questionId = (int)$_REQUEST['current_question_id'];
        $nextQuestionCount = (int)$_REQUEST['next_question_count'];
        //$currentQuestionCount = (int) $_REQUEST['current_question_count'];
        if (empty($this->model->getGivenAnswersByParticipantAndQuestionId($participant, $questionId))) {
            $answerIds = '0';
            if (!empty($_POST['answer'])) {
                $answerIds = implode(',', array_keys($_POST['answer']));
            }

            $this->model->setAnswers($questionId, $answerIds, $participant, $this->config['set_id']);
            header('Location: /?action=showQuestion&q=' . $nextQuestionCount);
        } else {
            header('Location: /?action=showAnswerFailure&q=' . $nextQuestionCount);
        }
    }

    /**
     * @return string
     */
    protected function showAnswerFailureAction(): string
    {
        return $this->view->setTemplate('Partials/failure.html')->setVariables([
            'q' => $_REQUEST['q']
        ])->render();
    }


    /**
     * @param $correctAnswersIds
     * @param $givenAnswerIds
     *
     * @return bool
     */
    private function evaluateAnswers($correctAnswersIds, $givenAnswerIds): bool
    {
        return (
            !empty(array_intersect($correctAnswersIds, $givenAnswerIds))
            && empty(array_diff($correctAnswersIds, $givenAnswerIds))
        );
    }

    /**
     * @return bool
     */
    private function checkSessionAndRedirect()
    {
        if(empty($_SESSION['quiz']['participant'])) {
            header('Location: /?action=showIntro');
        };
    }
}
