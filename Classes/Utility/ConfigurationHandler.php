<?php

namespace Classes\Utility;

use Symfony\Component\Yaml\Yaml;

class ConfigurationHandler
{
    /** @var array */
    private static $configuration = [];

    /** @var bool  */
    private static $isLoaded = false;

    private static function load()
    {
        self::$configuration = Yaml::parseFile('Config/Config.yaml');
        self::$isLoaded = true;
    }

    /**
     * @param string $type
     *
     * @return null
     */
    public static function getConfiguration(string $type)
    {
        if(self::$isLoaded === false) {
            self::load();
        }

        $return = null;
        if(!empty(self::$configuration[$type])) {
           $return = self::$configuration[$type];
        }

        return $return;
    }
}