<?php

namespace Classes\Utility;

use Classes\Model\QuizModel;
use Symfony\Component\Yaml\Yaml;

class TranslationHandler
{

    /**
     * @var string
     */
    private $currentLanguage = '';

    /**
     * @var array
     */
    private $languageCache = [];

    public function __construct()
    {
        $config = ConfigurationHandler::getConfiguration('QuizConfig');
        $model = new QuizModel(DatabaseHandler::getDatabaseConnection());
        $this->currentLanguage = $model->getQuestionSet($config['set_id'])[0]['language'];
        $this->languageCache = Yaml::parseFile('Language/Language.yaml');
    }

    /**
     * @param $key
     * @param $additionalValues
     *
     * @return string
     */
    public function translate($key, ...$additionalValues)
    {
        $return = $this->languageCache['en'][$key];
        if (empty($this->languageCache[$this->currentLanguage][$key])) {
            $return = $this->languageCache[$this->currentLanguage][$key];
        }

        if(!empty($additionalValues)) {
            $return = @vsprintf($return, $additionalValues) ?: '';
        }

        return $return;
    }
}