<?php

namespace Classes\Utility;

use Classes\Utility\ConfigurationHandler;

class DatabaseHandler
{
    /** @var \PDO */
    static private $dataBaseConnection = null;

    /** @var \Classes\Lib\Database null  */
    static private $instance = null;

    /**
     * @return \PDO
     */
    public static function getDatabaseConnection()
    {

        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance->getConnection();
    }

    /**
     * Connect to DB using PDO
     *
     * throws a Exception if connection fails
     */
    private function __construct()
    {
        $config = ConfigurationHandler::getConfiguration('Database');

        $hostAndDbName = sprintf('mysql:host=%s;dbname=%s', $config['host'], $config['database']);
        self::$dataBaseConnection = new \PDO($hostAndDbName, $config['user'], $config['password']);
        self::$dataBaseConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        self::$dataBaseConnection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $charset = 'utf8';
        if(!empty($config['charset'])) {
            $charset = $config['charset'];
        }
        self::$dataBaseConnection->query('SET NAMES \'' . $charset .'\'');
    }

    /**
     * @return \PDO
     */
    private function getConnection()
    {
        return self::$dataBaseConnection;
    }
}