<?php

namespace Classes\View;

use Classes\Utility\TranslationHandler;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class View
{
    /** @var null */
    private $renderer = null;

    /** @var array */
    private $variables = [];

    /** @var string */
    private $template = '';

    /** @var null  */
    private $translations = null;

    public function __construct()
    {
        $loader = new FilesystemLoader('Resources/Private/Templates');
//        $this->renderer = new Environment($loader, [
//            'cache' => 'Temp/Twig',
//        ]);
        // set to no cache for develop
        $this->renderer = new Environment($loader);
        $this->translations = new TranslationHandler();
    }

    /**
     * @param array $variables
     *
     * @return $this
     */
    public function setVariables(array $variables)
    {
        $this->variables = $variables;

        return $this;
    }

    /**
     * @param string $template
     *
     * @return $this
     */
    public function setTemplate(string $template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return string
     */
    public function render()
    {
        $variables = array_merge($this->variables, ['quiz' => $this->translations]);
        return $this->renderer->render($this->template, $variables);
    }
}