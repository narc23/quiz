<?php

namespace Classes\Model;

use Classes\Abstracts\AbstractModel;
use PDO;

class QuizModel extends AbstractModel
{
    /**
     * @param $setId
     * @param $randomize
     *
     * @return mixed
     */
    public function getQuestionSetIdsBySetId(int $setId, bool $randomize): array
    {
        $sql = 'SELECT id FROM questions WHERE question_set_id = :set_id';
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->bindValue(':set_id', $setId, PDO::PARAM_INT);
        $preparedQuery->execute();
        $result = $preparedQuery->fetchAll(\PDO::FETCH_NUM);

        $return = [];
        if (!empty($result)) {
            foreach ($result as $item) {
                $return[] = $item[0];
            }
        }

        if ($randomize === true) {
            shuffle($return);
        }


        return $return;
    }

    /**
     * @param $setId
     *
     * @return array
     */
    public function getQuestionSet(int $setId): array
    {
        $sql = 'SELECT * FROM question_sets WHERE id = :set_id';
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->bindValue(':set_id', $setId, PDO::PARAM_INT);
        $preparedQuery->execute();
        $result = $preparedQuery->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }


    /**
     * @param $questionId
     *
     * @return array
     */
    public function getAnswersByQuestionId(int $questionId): array
    {
        $sql = 'SELECT * FROM answers WHERE question_id = :question_id';
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->bindValue(':question_id', $questionId, PDO::PARAM_INT);
        $preparedQuery->execute();
        $result = $preparedQuery->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * @param $name
     * @param $setId
     *
     * @return int
     */
    public function setParticipant(string $name, int $setId): int
    {
        $sql = 'INSERT INTO participants (name, question_set_id, score) VALUES (:name, :question_set_id, 0)';
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->bindValue(':name', $name, PDO::PARAM_STR);
        $preparedQuery->bindValue(':question_set_id', $setId, PDO::PARAM_INT);
        $preparedQuery->execute();

        return (int)$this->databaseConnection->lastInsertId();
    }

    /**
     * @param $name
     * @param $setId
     *
     * @return array
     */
    public function getParticipant(string $name, int $setId): array
    {
        $sql = 'SELECT * FROM participants WHERE name = :name AND question_set_id = :question_set_id';
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->bindValue(':name', $name, PDO::PARAM_STR);
        $preparedQuery->bindValue(':question_set_id', $setId, PDO::PARAM_INT);
        $preparedQuery->execute();
        $result = $preparedQuery->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * @param $setId
     *
     * @return array
     */
    public function getAllParticipantsBySetId(int $setId): array
    {
        $sql = 'SELECT * FROM participants WHERE question_set_id = :question_set_id ORDER BY score DESC';
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->bindValue(':question_set_id', $setId, PDO::PARAM_INT);
        $preparedQuery->execute();
        $result = $preparedQuery->fetchAll(\PDO::FETCH_ASSOC);

        return $result;

    }

    /**
     * @param $questionId
     * @param $answerIds
     * @param $participantId
     * @param $questionSetId
     */
    public function setAnswers(int $questionId, string $answerIds, int $participantId, int $questionSetId)
    {
        $result = $this->getAnswersByQuestionId($questionId);

        $correctAnswersArray = [];
        foreach ($result as $item) {
            if ($item['correct'] == 1) {
                $correctAnswersArray[] = $item['id'];
            }
        }

        $correctAnswers = implode(',', $correctAnswersArray);

        $sql = 'INSERT INTO given_answers (participants_id, question_set_id, question_id, given_answer_ids, correct_answer_ids)
                VALUES (:participants_id, :question_set_id, :question_id, :given_answer_ids, :correct_answer_ids)';
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->bindValue(':participants_id', $participantId, PDO::PARAM_INT);
        $preparedQuery->bindValue(':question_set_id', $questionSetId, PDO::PARAM_INT);
        $preparedQuery->bindValue(':question_id', $questionId, PDO::PARAM_INT);
        $preparedQuery->bindValue(':given_answer_ids', $answerIds, PDO::PARAM_STR);
        $preparedQuery->bindValue(':correct_answer_ids', $correctAnswers, PDO::PARAM_STR);
        $preparedQuery->execute();
    }

    public function getGivenAnswersByParticipantAndQuestionId(int $participantId, int $questionId): array
    {
        $sql = 'SELECT * FROM given_answers WHERE participants_id = :participants_id AND question_id = :question_id';
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->bindValue(':participants_id', $participantId, PDO::PARAM_INT);
        $preparedQuery->bindValue(':question_id', $questionId, PDO::PARAM_INT);
        $preparedQuery->execute();
        $result = $preparedQuery->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * @param $participantId
     * @param $questionSetId
     * @param $score
     */
    public function setScore(int $participantId, int $questionSetId, int $score)
    {
        $sql = 'UPDATE participants SET score = :score WHERE id = :participants_id AND question_set_id= :question_set_id';
        $preparedQuery = $this->databaseConnection->prepare($sql);
        $preparedQuery->bindValue(':participants_id', $participantId, PDO::PARAM_INT);
        $preparedQuery->bindValue(':question_set_id', $questionSetId, PDO::PARAM_INT);
        $preparedQuery->bindValue(':score', $score, PDO::PARAM_INT);
        $preparedQuery->execute();
    }
}